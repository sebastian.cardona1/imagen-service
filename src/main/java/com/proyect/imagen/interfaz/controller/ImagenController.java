package com.proyect.imagen.interfaz.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.proyect.imagen.domain.entity.Imagen;
import com.proyect.imagen.infrastructure.service.ImagenService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/imagenes")
public class ImagenController {

	@Autowired
	private ImagenService iService;
	
	@Operation(summary = "Obtener todas las imagenes de la BD")
	@ApiResponses(value = { 
	  @ApiResponse(responseCode = "200", description = "Imagenes encontradas", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
	  @ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content), 
	  @ApiResponse(responseCode = "404", description = "Imagenes no encontradas", content = @Content),
	  @ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@GetMapping
	public ResponseEntity<List<Imagen>> getImagenes(){
		List<Imagen> imagenes = iService.getAll();
		if(imagenes.isEmpty()) {
			return ResponseEntity.noContent().build();
		}else {
			return ResponseEntity.ok(imagenes);
		}	
	}

	@Operation(summary = "Obtener la imagen JPEG de la clase imagen")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagen en JPEG encontrada", content = { @Content(mediaType = MediaType.IMAGE_JPEG_VALUE, schema = @Schema(implementation = byte.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagen en JPEG no encontrada", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@GetMapping(value = "imagen/{id}", produces = MediaType.IMAGE_JPEG_VALUE)
	public ResponseEntity<byte[]> getImagenBytes(@PathVariable("id") String id){
		byte[] imagen = iService.getImageByte(id);
		if(imagen!=null) {
			return ResponseEntity.ok(imagen);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Obtener una imagen de la BD a traves de su id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagen encontrada a traves de su id", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagen no encontrada a traves de su id", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@GetMapping("/{id}")
	public ResponseEntity<Imagen> getImagen(@PathVariable("id") String id){
		Imagen imagen = iService.getImageById(id);
		if(imagen!=null) {
			return ResponseEntity.ok(imagen);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Guardar una imagen en la BD")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagen guardada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagen no guardada encontrada", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@PostMapping
	public ResponseEntity<Imagen> saveImagen(@RequestParam(value = "personaId") int personaId, @RequestParam(value = "image") MultipartFile i){

		Imagen imagen = iService.save(i, personaId);
		return ResponseEntity.ok(imagen);
	}

	@Operation(summary = "Actualizar una imagen en la BD")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagen actualizada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagen no actualizada correctamente", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@PatchMapping("/{id}")
	public ResponseEntity<Imagen> updateImage(@PathVariable("id") String id, @RequestParam(value = "image") MultipartFile i, @RequestParam(value = "personaId") int personaId){
		Imagen imagen = iService.update(id, i, personaId);
		if(imagen!=null){
			return ResponseEntity.ok(imagen);
		}else{
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Eliminar una imagen en la BD")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagen eliminada correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagen no eliminada correctamente", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@DeleteMapping
	public ResponseEntity<Imagen> deleteImagen(@RequestBody Imagen i){
		Imagen imagen = iService.delete(i);
		if(imagen!=null) {
			return ResponseEntity.ok(imagen);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Eliminar una imagen en la BD por medio de su id")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagen eliminada correctamente a traves de su id", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagen no eliminada correctamente a traves de su id", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@DeleteMapping("/{id}")
	public ResponseEntity<Imagen> deleteImagenById(@PathVariable("id") String id){
		Imagen imagen = iService.deleteById(id);
		if(imagen!=null) {
			return ResponseEntity.ok(imagen);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

	@Operation(summary = "Imagenes asociadas a la id de una persona")
	@ApiResponses(value = {
			@ApiResponse(responseCode = "200", description = "Imagenes asociadas a la id de una persona obtenidas correctamente", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Imagen.class)) }),
			@ApiResponse(responseCode = "400", description = "Informacion invalida", content = @Content),
			@ApiResponse(responseCode = "404", description = "Imagenes no encontradas asociadas a la id de una persona", content = @Content),
			@ApiResponse(responseCode = "500", description = "Error inesperado", content = @Content)})
	@GetMapping("/persona/{id}")
	public ResponseEntity<List<Imagen>> getImagenByPersonaId(@PathVariable("id") int id){
		List<Imagen> imagenes = iService.byPersonaId(id);
		if(imagenes!=null){
			return ResponseEntity.ok(imagenes);
		}else{
			return ResponseEntity.noContent().build();
		}
	}
}

