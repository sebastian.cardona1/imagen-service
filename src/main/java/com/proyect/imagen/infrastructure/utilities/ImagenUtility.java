package com.proyect.imagen.infrastructure.utilities;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Base64;

public class ImagenUtility {
	public String encodeImage(String imgPath) throws Exception{
		
		FileInputStream  fStream = new FileInputStream(imgPath);
		byte[] image = fStream.readAllBytes();
		
		String imageString = Base64.getEncoder().encodeToString(image);
		fStream.close();
		
		return imageString;
		
	}
	
	public byte[] decodeImage(String txtPath) throws Exception {
		byte[] image = Base64.getDecoder().decode(new String(txtPath));
		return image;
	}
	
}
