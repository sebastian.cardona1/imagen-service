package com.proyect.imagen.infrastructure.service;

import java.io.IOException;
import java.lang.module.ResolutionException;
import java.util.List;
import java.util.UUID;

import com.proyect.imagen.infrastructure.exceptions.NoDataFoundException;
import com.proyect.imagen.infrastructure.exceptions.ResourceNotFoundException;
import com.proyect.imagen.infrastructure.repository.ImagenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyect.imagen.domain.entity.Imagen;
import com.proyect.imagen.infrastructure.utilities.ImagenUtility;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class ImagenService {

	@Autowired
	private ImagenRepository iRepository;
	
	private ImagenUtility iUtility;

	public ImagenService() {
		iUtility = new ImagenUtility();
	}

	public List<Imagen> getAll(){
		List<Imagen> imagenes = iRepository.findAll();
		if (imagenes!=null){
			return imagenes;
		}else{
			throw new NoDataFoundException("Las imagenes no se pudieron cargar correctamente");
		}
	}

	public Imagen getImageById(String id) {
		Imagen imagen = iRepository.findById(id).orElse(null);
		if(imagen!=null){
			return imagen;
		}else{
			throw new NoDataFoundException("La imagen a actualizar con id "+id+" no fue encontrada");
		}
	}

	public byte[] getImageByte(String id){
		Imagen imagen = iRepository.findById(id).orElse(null);
		if(imagen!=null ){
			return imagen.getImagen();
		}else{
			throw new NoDataFoundException("La imagen con id "+id+" no pudo ser procesada correctamente");
		}
	}

	@Transactional
	public Imagen save(MultipartFile i, int personaId) {
		Imagen imagen = new Imagen();
		UUID uuid = UUID.randomUUID();
		try {
			imagen.setImagen(i.getBytes());
			imagen.setId(uuid.toString());
			imagen.setPersonaId(personaId);
		} catch (IOException e) {
			throw new RuntimeException("La imagen no pudo ser guardada correctamente");
		}
		return iRepository.save(imagen);
	}

	@Transactional
	public Imagen delete(Imagen i) {
		Imagen imagen = iRepository.findById(i.getId()).orElse(null);
		if(imagen!=null) {
			iRepository.delete(i);
			return imagen;
		}else {
			throw new NoDataFoundException("La imagen a eliminar no fue encontrada");
		}
	}

	@Transactional
	public Imagen update(String id, MultipartFile i, int personaId){
		Imagen imagen = iRepository.findById(id).orElse(null);
		if(imagen!=null){
			try {
				imagen.setImagen(i.getBytes());
				imagen.setPersonaId(personaId);
			} catch (IOException e) {
				throw new RuntimeException("La imagen no pudo ser actualizada correctamente la imagen");
			}
			iRepository.save(imagen);
			return imagen;
		}else{
			throw new NoDataFoundException("La imagen a actualizar con id "+id+" no fue encontrada");
		}
	}

	@Transactional
	public Imagen deleteById(String id) {
		Imagen imagen = iRepository.findById(id).orElse(null);
		if(imagen!=null) {
			iRepository.deleteById(id);
			return imagen;
		}else {
			throw new NoDataFoundException("La imagen no fue encontrada con id "+id+" para ser eliminada");
		}
	}

	public List<Imagen> byPersonaId(int personaId){
		List<Imagen> imagenes = iRepository.findImagenByPersonaId(personaId);
		if(imagenes!=null){
			return imagenes;
		}else{
			throw new ResourceNotFoundException("Las imagenes asociadas con el personaId "+personaId+" no fue encontrado");
		}
	}
}
