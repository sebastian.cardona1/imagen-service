package com.proyect.imagen.infrastructure.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.proyect.imagen.domain.entity.Imagen;

import java.util.List;

@Repository
public interface ImagenRepository extends MongoRepository<Imagen, String>{

    List<Imagen> findImagenByPersonaId(int personaId);
}
