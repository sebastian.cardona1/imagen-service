package com.proyect.imagen.domain.entity;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Document(collection = "images")
public class Imagen {

	@Id
	private String id;
	private byte[] imagen;
	private int personaId;
	
}
