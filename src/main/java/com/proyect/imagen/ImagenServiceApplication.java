package com.proyect.imagen;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImagenServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImagenServiceApplication.class, args);
	}

}
