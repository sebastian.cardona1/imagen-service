package com.proyect.imagen.infrastructure.service;

import com.proyect.imagen.domain.entity.Imagen;
import com.proyect.imagen.infrastructure.repository.ImagenRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class ImagenServiceTest {
    @InjectMocks
    ImagenService iService;

    @Mock
    ImagenRepository iRepository;

    Imagen imagen;

    private static final String ID = "b70484b7-1ef7-4c76-830e-c9ffddbfad4f";

    private static final int ID_PERSONA = 1;

    private static final int SIZE_BYTE = 1;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        imagen = new Imagen();
        imagen.setId(ID);
        imagen.setPersonaId(ID_PERSONA);
        imagen.setImagen(new byte[SIZE_BYTE]);
    }

    @Test
    void getAll() {
        when(iService.getAll()).thenReturn(Arrays.asList(imagen));
        assertEquals( iRepository.findAll().size(), 1);
    }

    @Test
    void getImageById() {
        when(iRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(imagen));
        MultipartFile m = mock(MultipartFile.class);
        var result = iService.getImageById(ID);
        assertEquals(ID, result.getId());
    }

    @Test
    void getImageByte() {
        when(iRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(imagen));
        MultipartFile m = mock(MultipartFile.class);
        var result = iService.getImageByte(ID);
        assertEquals(SIZE_BYTE, result.length);
    }

    @Test
    void save() {
        when(iRepository.save(Mockito.any())).thenReturn(imagen);
        MultipartFile m = mock(MultipartFile.class);
        var result = iService.save(m, ID_PERSONA);
        assertEquals(ID, result.getId());
    }

    @Test
    void delete() {
        when(iRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(imagen));
        MultipartFile m = mock(MultipartFile.class);
        var result = iService.delete(imagen);
        assertEquals(ID, result.getId());
    }

    @Test
    void update(){
        when(iRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(imagen));
        MultipartFile m = mock(MultipartFile.class);
        var result = iService.update(ID, m,ID_PERSONA);
        assertEquals(ID, result.getId());
    }

    @Test
    void deleteById() {
        when(iRepository.findById(Mockito.any())).thenReturn(Optional.ofNullable(imagen));
        MultipartFile m = mock(MultipartFile.class);
        var result = iService.deleteById(ID);
        assertEquals(ID, result.getId());
    }

    @Test
    void byPersonaId() {
        iService.byPersonaId(ID_PERSONA);
        verify(iRepository).findImagenByPersonaId(ID_PERSONA);
    }
}