package com.proyect.imagen.interfaz.controller;

import com.proyect.imagen.domain.entity.Imagen;
import com.proyect.imagen.infrastructure.service.ImagenService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class ImagenControllerTest {

    @InjectMocks
    ImagenController iController;

    @Mock
    ImagenService iService;

    @Mock
    Imagen imagen;

    @Mock
    MultipartFile multipartFile;

    static byte[] imagenByte;

    static List<Imagen> imagenes;

    private static final String ID = "b70484b7-1ef7-4c76-830e-c9ffddbfad4f";

    private static final int ID_PERSONA = 1;

    @BeforeAll
    static void context(){
        imagenByte = new byte[10];
        imagenes = new ArrayList<>();
    }

    @Test
    void getImagenes() {
        List<Imagen> imagenes = new ArrayList<>();
        imagenes.add(imagen);
        when(iService.getAll()).thenReturn(imagenes);
        assertEquals(iController.getImagenes().getBody().size(), imagenes.size());
    }

    @Test
    void getImagenBytes() {
        when(iService.getImageByte(ID)).thenReturn(imagenByte);
        assertEquals(iController.getImagenBytes(ID).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void getImagen() {
        when(iService.getImageById(ID)).thenReturn(imagen);
        assertEquals(iController.getImagen(ID).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void saveImagen() throws IOException {
        assertEquals(iController.saveImagen(ID_PERSONA,multipartFile).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void updateImage() throws IOException {
        when(iService.update(ID, multipartFile, ID_PERSONA)).thenReturn(imagen);
        assertEquals(iController.updateImage(ID,multipartFile,ID_PERSONA).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void deleteImagen() {
       when(iService.delete(imagen)).thenReturn(imagen);
       assertEquals(iController.deleteImagen(imagen).getStatusCode(), HttpStatus.OK);
    }

    @Test
    void deleteImagenById() {
        when(iService.getImageById(ID)).thenReturn(null);
        assertEquals(iController.deleteImagenById(ID).getStatusCode(), HttpStatus.NOT_FOUND);
    }

    @Test
    void getImagenByPersonaId() {
        when(iService.byPersonaId(ID_PERSONA)).thenReturn(imagenes);
        assertEquals(iController.getImagenByPersonaId(ID_PERSONA).getStatusCode(), HttpStatus.OK);
    }
}